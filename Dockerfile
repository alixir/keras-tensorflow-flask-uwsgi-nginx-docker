FROM tiangolo/uwsgi-nginx-flask:python3.6

ENV UWSGI_INI /app/uwsgi.ini

COPY ./app /app

RUN pip install uuid Pillow matplotlib h5py tensorflow keras
